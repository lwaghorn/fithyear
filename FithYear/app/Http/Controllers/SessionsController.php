<?php

namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;
use App\SensorOneDataValue;

class SessionsController extends Controller
{
    public function index(){
        $sessions =  Session::where('id','>',-1)->get();
        $date = $sessions->first();
        $readable = date('m-d', strtotime($date));
        $response = [
            'data' => $sessions
        ];
        return $response;
    }


    public function getSessionData(Request $request){
        $session_id = $request->input('data');
        $session = Session::where('id','=',$session_id)->first();


        $sensorOneData = $session->sensorOneDataValues();

        $row = $sensorOneData->first();

        info($row);

        $response = [
            'status'=>'success'
        ];

        return $response;
    }

}
