<?php

namespace App\Http\Controllers;

use App\SensorOneDataValue;
use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function sessions(){

        return view('sessions');

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalSessions = Session::count();
        $totalDataRead = SensorOneDataValue::count();
        $totalFlaggedSessions = Session::where('flagged','=', 1 )->count();

        info('ARDUINO RECIEVED 2');

        return view('home', [
            'totalSessions' => $totalSessions,
            'totalDataRead'=>$totalDataRead,
            'totalFlaggedSessions'=> $totalFlaggedSessions
        ]);
    }

    public function arduinoIndex(Request $request){
        info('HOLYFUCKITWORKED  ' . $request);
    }

    public function timeline(){

        $perday = Session::select([
            // This aggregates the data and makes available a 'count' attribute
            DB::raw('count(id) as `count`'),
            // This throws away the timestamp portion of the date
            DB::raw('DATE(created_at) as day')
            // Group these records according to that day
        ])->groupBy('day')->get();

        $response = array(
            'status' => 'success',
            'perDay' => $perday,
        );

        return $response;

    }





}
