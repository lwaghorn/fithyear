<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = ['name', 'measure_type', 'measure', 'user_id','main_item_id','sequence'];

    public function AddSensorOne()
    {
        $this->has_sensor_one = 1;
        $this->save();
        return ;
    }
    public function AddSensorTwo()
    {
        $this->has_sensor_one = 1;
        $this->save();
        return ;
    }

    public function sensorOneDataValues()
    {
        return $this->hasMany('App\SensorOneDataValue', 'session_id');
    }
}
