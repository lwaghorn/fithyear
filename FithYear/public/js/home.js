$( document ).ready(function() {

$('.navpill').removeClass('active');
$('#homeNavPill').addClass('active');

    var data;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "GET",
        url: '/home/sessionTimeline',
        dataType: 'json',
        success: function(response) {
            data = response.perDay;

            var labels = [];
            var count = [];
            for (var i in data){
                labels.push(data[i].day);
                count.push(data[i].count);
            }

            var lineChartData = {
                labels: labels,
                datasets: [{
                    label: labels,
                    fillColor: 'rgba(26,188,156,0.5)',
                    strokeColor: 'rgba(26,188,156,1)',
                    pointColor: 'rgba(220,220,220,1)',
                    pointStrokeColor: '#fff',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: count
                }]

            };
            var ctx1 = document.getElementById("line").getContext("2d");
            window.myLine = new Chart(ctx1).Line(lineChartData, {
                responsive: true
            });

            },
        error: function (ingredients) {
            console.log('Error:');
        }
    });

});