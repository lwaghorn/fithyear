$(document).ready(function() {

    $('.navpill').removeClass('active');
    $('#sessionNavPill').addClass('active');


    $('#sessionsTable').DataTable( {

        "ajax": "sessions/index",
        "rowId":'id',
        "columns": [
            { "data": "created_at" },
            { "data": "notes" },
            { "data": "flagged" },
            { "data": "has_sensor_one" },
            { "data": "has_sensor_two" },
            { "data": "has_sensor_three" },
            {"data": null}
        ],
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<button class='getSessionBtn'>Click!</button>",
        },{
                "targets": [ 2,3,4,5 ],
                "visible": false
            }]
    } );

    $('#sessionsTable tbody').on( 'click', 'button', function () {
        var id = $(this).closest('tr').attr('id');
        showSessionData(id);
    } );







    $('.getSessionBtn').on('click',function () {

        document.querySelector('.ct-chart-2').__chartist__.update();
        console.log('updated');
    });

} );




function showSessionData(id) {

$('#sessionModal').modal('toggle');

console.log("from js "+ id );

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "GET",
        url: '/sessions/getSessionData',
        dataType: 'json',
        data: {
            data : id
        },
        success: function(response) {

        },
        error: function (ingredients) {
            console.log('Error:');
        }
    });

    var data;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "GET",
        url: '/home/sessionTimeline',
        dataType: 'json',
        success: function(response) {
            data = response.perDay;

            var labels = [];
            var count = [];
            for (var i in data){
                labels.push(data[i].day);
                count.push(data[i].count);
            }

            var lineChartData = {
                labels: labels,
                datasets: [{
                    label: labels,
                    fillColor: 'rgba(26,188,156,0.5)',
                    strokeColor: 'rgba(26,188,156,1)',
                    pointColor: 'rgba(220,220,220,1)',
                    pointStrokeColor: '#fff',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: count
                }]

            };
            var ctx1 = document.getElementById("myChart").getContext("2d");
            window.myLine = new Chart(ctx1).Line(lineChartData, {
                responsive: true
            });

        },
        error: function (ingredients) {
            console.log('Error:');
        }
    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "GET",
        url: '/home/sessionTimeline',
        dataType: 'json',
        success: function(response) {
            data = response.perDay;

            var labels = [];
            var count = [];
            for (var i in data){
                labels.push(data[i].day);
                count.push(data[i].count);
            }

            var lineChartData = {
                labels: labels,
                datasets: [{
                    label: labels,
                    fillColor: 'rgba(26,188,156,0.5)',
                    strokeColor: 'rgba(26,188,156,1)',
                    pointColor: 'rgba(220,220,220,1)',
                    pointStrokeColor: '#fff',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: count
                }]

            };
            var ctx1 = document.getElementById("myChart3").getContext("2d");
            window.myLine = new Chart(ctx1).Line(lineChartData, {
                responsive: true
            });

        },
        error: function (ingredients) {
            console.log('Error:');
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "GET",
        url: '/home/sessionTimeline',
        dataType: 'json',
        success: function(response) {
            data = response.perDay;

            var labels = [];
            var count = [];
            for (var i in data){
                labels.push(data[i].day);
                count.push(data[i].count);
            }

            var lineChartData = {
                labels: labels,
                datasets: [{
                    label: labels,
                    fillColor: 'rgba(26,188,156,0.5)',
                    strokeColor: 'rgba(26,188,156,1)',
                    pointColor: 'rgba(220,220,220,1)',
                    pointStrokeColor: '#fff',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: count
                }]

            };
            var ctx1 = document.getElementById("myChart4").getContext("2d");
            window.myLine = new Chart(ctx1).Line(lineChartData, {
                responsive: true
            });

        },
        error: function (ingredients) {
            console.log('Error:');
        }
    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "GET",
        url: '/home/sessionTimeline',
        dataType: 'json',
        success: function(response) {
            data = response.perDay;

            var labels = [];
            var count = [];
            for (var i in data){
                labels.push(data[i].day);
                count.push(data[i].count);
            }

            var lineChartData = {
                labels: labels,
                datasets: [{
                    label: labels,
                    fillColor: 'rgba(26,188,156,0.5)',
                    strokeColor: 'rgba(26,188,156,1)',
                    pointColor: 'rgba(220,220,220,1)',
                    pointStrokeColor: '#fff',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: count
                }]

            };
            var ctx1 = document.getElementById("myChart2").getContext("2d");
            window.myLine = new Chart(ctx1).Line(lineChartData, {
                responsive: true
            });

        },
        error: function (ingredients) {
            console.log('Error:');
        }
    });


}

