#include <ArduinoJson.h>
#define trigPin 13
#define echoPin 12

void setup() {
   Serial.begin (9600);
   pinMode(trigPin, OUTPUT);
   pinMode(echoPin, INPUT);
  
}

void loop() {
   long duration, distance;
  StaticJsonBuffer<200> jsonBuffer;

   // Create the root object
  JsonObject& root = jsonBuffer.createObject();
  
  JsonArray& contents = root.createNestedArray("contents");
  JsonObject& nested = contents.createNestedObject();    
  nested["sensorId"] = 1 ;
  
  for(int i = 0 ; i<10 ; i++){
  
  JsonObject& nested = contents.createNestedObject();    
  
  digitalWrite(trigPin, LOW);  
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW);
  
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;  
  
  nested["data"] = distance;
  nested["time"]= millis()/1000;
 
  delay(1000);
    
    }
  
  
  
  
  root.printTo(Serial);

  while(1){}

 // Serial.println(array.success());

//Serial.println(nested.success());
  
}
