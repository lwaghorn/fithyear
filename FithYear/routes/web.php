<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Home Routes
Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@arduinoIndex');
Route::post('/index', 'HomeController@arduinoIndex');
Route::get('/home/sessionTimeline', 'HomeController@timeline');

//Session Routes
Route::get('/sessions', 'HomeController@sessions');
Route::get('/sessions/index', 'SessionsController@index');
Route::get('/sessions/getSessionData', 'SessionsController@getSessionData');

//Sensor Attendance
Route::get('/attendance/present','AttendanceController@present');
Route::get('/attendance','AttendanceController@index');
Route::get('attendance/test','AttendanceController@test');
Route::get('attendance/getAttendance','AttendanceController@getAttendance');
