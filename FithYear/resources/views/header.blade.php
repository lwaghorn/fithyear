<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'HyperLoop') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- PubNub SDK -->
    <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.18.0.js"></script>


    <link type="text/css" rel="stylesheet" href="//pubnub.github.io/eon/v/eon/1.0.0/eon.css" />



    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico')}}" type="image/x-icon">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Fonts  -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css')}}" >
    <link rel="stylesheet" href="{{ asset('assets/css/simple-line-icons.css')}}">
    <!-- CSS Animate -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}">
    <!-- Daterange Picker -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- Calendar demo -->
    <link rel="stylesheet" href="{{ asset('assets/css/clndr.css')}}">
    <!-- Switchery -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/switchery.min.css')}}">
    <!-- Custom styles for this theme -->
    <link rel="stylesheet" href="{{ asset('assets/css/main.css')}}">
    <!-- Feature detection -->
    <script src="{{ asset('assets/js/vendor/modernizr-2.6.2.min.js')}}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--/Config demo-->
    <script src="{{ asset('js/pubnub.js') }}"></script>
    <script src="{{ asset('D3/d3/d3.min.js') }}"></script>
    <script src="{{ asset('C3/c3-0.4.18/c3.min.js') }}"></script>
    <script type="text/javascript" src="//pubnub.github.io/eon/v/eon/1.0.0/eon.js"></script>

    <!--Global JS-->
    <script src="{{ asset('assets/js/vendor/jquery-1.11.1.min.js') }}"></script>
    <script src=" {{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src=" {{ asset('assets/plugins/chartjs/Chart.min.js') }}"></script>

    <!--[if lt IE 9]>
    <script src="{{asset('assets/js/vendor/html5shiv.js')}}"></script>
    <script src="{{asset('assets/js/vendor/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body>
<section id="main-wrapper" class="theme-default">
    <header id="header">
        <!--logo start-->
        <div class="brand">
            <a href="index.html" class="logo">
                <span>Facilities</span>DataBase</a>
        </div>
        <!--logo end-->
        <ul class="nav navbar-nav navbar-left">
            <li class="toggle-navigation toggle-left">
                <button class="sidebar-toggle" id="toggle-left">
                    <i class="fa fa-bars"></i>
                </button>
            </li>
            <li class="toggle-profile hidden-xs">
                <button type="button" class="btn btn-default" id="toggle-profile">
                    <i class="icon-user"></i>
                </button>
            </li>
            <li class="hidden-xs">
                <input type="text" class="search" placeholder="Search project...">
                <button type="submit" class="btn btn-sm btn-search"><i class="fa fa-search"></i>
                </button>
            </li>
        </ul>
    </header>
    <!--sidebar left start-->
    <aside class="sidebar sidebar-left">
        <div class="sidebar-profile">
            <div class="avatar">
                <img class="profile-image" src="{{ asset('images/paradigm-hyperloop.png') }}" alt="profile">
                <i class="on border-dark animated bounceIn"></i>
            </div>
            <div class="profile-body dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><h4>Liam Waghorn<span class="caret"></span></h4></a>
                <small class="title">Small Cheese Big Pizza</small>
                <ul class="dropdown-menu animated fadeInRight" role="menu">
                    <li>
                        <a href="javascript:void(0);">
                                <span class="icon"><i class="fa fa-user"></i>
                                </span>My Account</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                                <span class="icon"><i class="fa fa-cog"></i>
                                </span>Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);">
                                <span class="icon"><i class="fa fa-sign-out"></i>
                                </span>Logout</a>
                    </li>
                </ul>
            </div>
        </div>
        <nav>
            <h5 class="sidebar-header">Navigation</h5>
            <ul class="nav nav-pills nav-stacked">
                <li class="navpill" id="homeNavPill">
                    <a href="/" title="Home">
                        <i class="icon-home"></i> Home
                    </a>
                </li>
                <li class="navpill" id="sessionNavPill">
                    <a href="/sessions" title="sessions">
                        <i class="icon-briefcase"></i> Sessions
                    </a>
                </li>
                <li class="navpill" id="fakeAttendanceNavPill">
                    <a href="/attendance/test" title="">
                        <i class="icon-call-out"></i> Fake Data
                    </a>
                </li>
                <li class="navpill" id="fakeAttendanceNavPill">
                    <a href="/attendance" title="">
                        <i class="icon-wrench"></i> Temp Page
                    </a>
                </li>
                <li class="nav-dropdown">
                    <a href="#" title="UI Elements">
                        <i class="fa fa-fw fa-file-text"></i> Pages
                    </a>
                    <ul class="nav-sub">
                        <li>
                            <a href="" title="Buttons">
                                Sessions
                            </a>
                        </li>
                        <li>
                            <a href="" title="Sliders &amp; Progress">
                                 Collect
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </aside>
    <!--sidebar left end-->



                            @yield('content')


    </section>
    <!--main content end-->
</section>
<!--sidebar right start-->
<aside id="sidebar-right">
    <h4 class="sidebar-title">contact List</h4>
    <div id="contact-list-wrapper">
        <div class="heading">
            <ul>
                <li class="new-contact"><a href="javascript:void(0)"><i class="fa fa-plus"></i></a>
                </li>
                <li>
                    <input type="text" class="search" placeholder="Search">
                    <button type="submit" class="btn btn-sm btn-search"><i class="fa fa-search"></i>
                    </button>
                </li>
            </ul>
        </div>
        <div id="contact-list">

        </div>
        <div id="contact-user">
            <div class="chat-user active"><span><i class="icon-bubble"></i></span>
            </div>
            <div class="email-user"><span><i class="icon-envelope-open"></i></span>
            </div>
            <div class="call-user"><span><i class="icon-call-out"></i></span>
            </div>
        </div>
    </div>
</aside>
<!--/sidebar right end-->






    <script src=" {{ asset('assets/plugins/navgoco/jquery.navgoco.min.js') }}"></script>
    <script src=" {{ asset('assets/plugins/pace/pace.min.js') }}"></script>
    <script src=" {{ asset('assets/plugins/fullscreen/jquery.fullscreen-min.js') }}"></script>
    <script src=" {{ asset('assets/js/src/app.js') }}"></script>
    <!--Page Level JS-->
    <script src=" {{ asset('assets/plugins/countTo/jquery.countTo.js') }}"></script>
    <script src=" {{ asset('assets/plugins/weather/js/skycons.js') }}"></script>
    <script src=" {{ asset('assets/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src=" {{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>





</body>

<script>
    var pubnub = new PubNub({
        publishKey: 'pub-c-97b65681-bfde-4d5b-b232-72e6bdce769c',
        subscribeKey: 'sub-c-9673c532-d882-11e7-96e5-36e8e28b70d2'
    });

    eon.chart({
        pubnub: pubnub,
        channels: ["c3-spline"],
        generate: {
            bindto: '#chart',
            data: {
                labels: true
            }
        }
    });

    /*setInterval(function(){
        pubnub.publish({
            channel: 'c3-spline',
            message: {
                eon: {
                    'Austin': Math.floor(Math.random() * 99),
                    'New York': Math.floor(Math.random() * 99),
                    'San Francisco': Math.floor(Math.random() * 99),
                    'Portland': Math.floor(Math.random() * 99)
                }
            }
        });

    }, 1000);*/



</script>


</html>
