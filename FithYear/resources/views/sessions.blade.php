@extends('header')

@section('content')
    <!--main content start-->
    <section class="main-content-wrapper">
        <div class="pageheader">
            <h1>Sessions</h1>
            <p class="description">Browse, filter and search all your previous session</p>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li class="active">Sessions</li>
                </ol>
            </div>
        </div>
        <section id="main-content">




            <div class="row">
                <div class="col-md-12">
                        <button type="button" class="btn btn-default btn-3d">Default</button>
                        <button type="button" class="btn btn-primary btn-3d">Primary</button>
                        <button type="button" class="btn btn-success btn-3d">Success</button>
                        <button type="button" class="btn btn-info btn-3d">Info</button>
                        <button type="button" class="btn btn-warning btn-3d">Warning</button>
                        <button type="button" class="btn btn-danger btn-3d">Danger</button>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body ng-binding">

                            <table id="sessionsTable" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Extn.</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                    <th>button</th>
                                </tr>
                                </thead>
                                <tfoot>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </section>


    <!-- Basic Modal -->
    <div class="modal fade" id="sessionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog sessionModalDialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <!-- START SENSOR -->
                            <div class="col-md-4 col-sm-6">
                                <div class="row">
                                    <!-- START HEADING PANEL -->
                                        <div class="panel panel-primary">
                                            <div class="panel-heading text-center">
                                                <h3 class="panel-title">Sensor One</h3>
                                            </div>
                                            <div class="panel-body">
                                                {{-- CHART.JS--}}
                                                <div style="margin-left: 5px ; margin-right: -5px">
                                                    <canvas id="myChart"></canvas>

                                                </div>
                                            </div>
                                        </div>
                                    <!-- STOP HEADING PANEL -->





                                    <!-- START OVERVIEW PANEL -->
                                        <div class="panel panel-solid-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Overview</h3>
                                            </div>
                                            <div class="panel-body text-center">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="row">
                                                            <strong>Key Value:</strong> 33
                                                        </div>
                                                        <div class="col-row-6">
                                                            <strong>Key Value:</strong> 23
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="row">
                                                            <strong>Key Value:</strong> 33
                                                        </div>
                                                        <div class="col-row-6">
                                                            <strong>Key Value:</strong> 23
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- STOP OVERVIEW PANEL -->
                                    <hr>
                                    <!-- START BUTTONS ROW -->
                                        <div class="row">
                                            <div class=" col-xs-6 col-xs-push-3">
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <span class="sensor-action-icon icon-tag"></span>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <span class=" sensor-action-icon icon icon-arrow-down"></span>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <span class="icon-list sensor-action-icon"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- END BUTTONS ROW -->
                                </div>
                            </div>
                        <!-- END SENSOR -->
                        <!-- START SENSOR -->
                        <div class="col-md-4 col-sm-6">
                            <div class="row">
                                <!-- START HEADING PANEL -->
                                <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Sensor One</h3>
                                    </div>
                                    <div class="panel-body">
                                        {{-- CHART.JS--}}
                                        <div style="margin-left: 5px ; margin-right: -5px">
                                            <canvas id="myChart2"></canvas>

                                        </div>
                                    </div>
                                </div>
                                <!-- STOP HEADING PANEL -->





                                <!-- START OVERVIEW PANEL -->
                                <div class="panel panel-solid-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Overview</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <strong>Key Value:</strong> 33
                                                </div>
                                                <div class="col-row-6">
                                                    <strong>Key Value:</strong> 23
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <strong>Key Value:</strong> 33
                                                </div>
                                                <div class="col-row-6">
                                                    <strong>Key Value:</strong> 23
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- STOP OVERVIEW PANEL -->
                                <hr>
                                <!-- START BUTTONS ROW -->
                                <div class="row">
                                    <div class=" col-xs-6 col-xs-push-3">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <span class="sensor-action-icon icon-tag"></span>
                                            </div>
                                            <div class="col-xs-4">
                                                <span class=" sensor-action-icon icon icon-arrow-down"></span>
                                            </div>
                                            <div class="col-xs-4">
                                                <span class="icon-list sensor-action-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END BUTTONS ROW -->
                            </div>
                        </div>
                        <!-- END SENSOR -->
                        <!-- START SENSOR -->
                        <div class="col-md-4 col-sm-6">
                            <div class="row">
                                <!-- START HEADING PANEL -->
                                <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Sensor One</h3>
                                    </div>
                                    <div class="panel-body">
                                        {{-- CHART.JS--}}
                                        <div style="margin-left: 5px ; margin-right: -5px">
                                            <canvas id="myChart3"></canvas>

                                        </div>
                                    </div>
                                </div>
                                <!-- STOP HEADING PANEL -->





                                <!-- START OVERVIEW PANEL -->
                                <div class="panel panel-solid-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Overview</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <strong>Key Value:</strong> 33
                                                </div>
                                                <div class="col-row-6">
                                                    <strong>Key Value:</strong> 23
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <strong>Key Value:</strong> 33
                                                </div>
                                                <div class="col-row-6">
                                                    <strong>Key Value:</strong> 23
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- STOP OVERVIEW PANEL -->
                                <hr>
                                <!-- START BUTTONS ROW -->
                                <div class="row">
                                    <div class=" col-xs-6 col-xs-push-3">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <span class="sensor-action-icon icon-tag"></span>
                                            </div>
                                            <div class="col-xs-4">
                                                <span class=" sensor-action-icon icon icon-arrow-down"></span>
                                            </div>
                                            <div class="col-xs-4">
                                                <span class="icon-list sensor-action-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END BUTTONS ROW -->
                            </div>
                        </div>
                        <!-- END SENSOR -->
                     {{--   <!-- START SENSOR -->
                        <div class="col-md-4 col-sm-6">
                            <div class="row">
                                <!-- START HEADING PANEL -->
                                <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                        <h3 class="panel-title">Sensor One</h3>
                                    </div>
                                    <div class="panel-body">
                                        --}}{{-- CHART.JS--}}{{--
                                        <div style="margin-left: 5px ; margin-right: -5px">
                                            <canvas id="myChart4"></canvas>

                                        </div>
                                    </div>
                                </div>
                                <!-- STOP HEADING PANEL -->





                                <!-- START OVERVIEW PANEL -->
                                <div class="panel panel-solid-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Overview</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <strong>Key Value:</strong> 33
                                                </div>
                                                <div class="col-row-6">
                                                    <strong>Key Value:</strong> 23
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <strong>Key Value:</strong> 33
                                                </div>
                                                <div class="col-row-6">
                                                    <strong>Key Value:</strong> 23
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- STOP OVERVIEW PANEL -->
                                <hr>
                                <!-- START BUTTONS ROW -->
                                <div class="row">
                                    <div class=" col-xs-6 col-xs-push-3">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <span class="sensor-action-icon icon-tag"></span>
                                            </div>
                                            <div class="col-xs-4">
                                                <span class=" sensor-action-icon icon icon-arrow-down"></span>
                                            </div>
                                            <div class="col-xs-4">
                                                <span class="icon-list sensor-action-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END BUTTONS ROW -->
                            </div>
                        </div>
                        <!-- END SENSOR -->--}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Basic Modal -->



<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>

<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('css/sessions.css') }}" >

        <script src="{{asset('js/sessions.js')}}"></script>

@endsection
