@extends('header')

@section('content')
    <!--main content start-->
    <section class="main-content-wrapper">
        <div class="pageheader">
            <h1>Send Fake Attendance</h1>
            <p class="description">Send a sensor ID to the attendance controller to test the attendance functionality</p>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li class="active"> Attendance > Fake Submission</li>
                </ol>
            </div>
        </div>
        <section id="main-content">
            <div class="row">

                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sensor</h3>
                            <div class="actions pull-right">

                            </div>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="sensorID" class="col-sm-2 control-label">Sensor #</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="sensorID" placeholder="XXX">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary center-block" onclick="submitSensor()">Submit</button>
                                    </div>
                                </div>

                                <div class="alert alert-success alert-dismissable animated " id="sensorAddSuccessNotification">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <strong>Done!</strong> Sensor checked in
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </section>


        <script src="{{asset('js/fakeAttendance.js')}}"></script>

@endsection
