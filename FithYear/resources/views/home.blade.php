@extends('header')

@section('content')
    <!--main content start-->
    <section class="main-content-wrapper">
        <div class="pageheader">
            <h1>Home</h1>
            <p class="description">Welcome to the Paradigm Hyperloop Data Acquisition Dashboard</p>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li class="active">Home</li>
                </ol>
            </div>
        </div>
        <!--main content start-->
        <section id="main-content" class="animated fadeInUp">

            <!-- START FIRST ROW -->
            <div class="row">
                <!-- START 4 PANEL WRAPPER -->
                <!-- Half at medium - Third at large - Width at XS -->
                <div class="col-md-12 col-lg-6">
                    <div class="row">
                        <!-- Total Sessions -->
                        <div class="col-md-6">
                            <div class="panel panel-solid-success widget-mini">
                                <div class="panel-body">
                                    <i class="icon-rocket"></i>
                                    <span class="total text-center" id="totalSessions">{{ $totalSessions}}</span>
                                    <span class="title text-center">Sessions Ran</span>
                                </div>
                            </div>
                        </div>
                        <!-- Favorited Sessions -->
                        <div class="col-md-6">
                            <div class="panel widget-mini">
                                <div class="panel-body">
                                    <i class="icon-flag"></i>
                                    <span class="total text-center" id="favoritedSessions">{{ $totalFlaggedSessions }}</span>
                                    <span class="title text-center">Favorited Sessions</span>
                                </div>
                            </div>
                        </div>
                        <!-- Total DataPoints Collected -->
                        <div class="col-md-6">
                            <div class="panel widget-mini">
                                <div class="panel-body">
                                    <i class="icon-bar-chart"></i>
                                    <span class="total text-center" id="totalDataPointsCollected">{{$totalDataRead}}</span>
                                    <span class="title text-center">Data Values Collected</span>
                                </div>
                            </div>
                        </div>
                        <!-- Fucks Given -->
                        <div class="col-md-6">
                            <div class="panel panel-solid-info widget-mini">
                                <div class="panel-body">
                                    <i class="icon-fire"></i>
                                    <span class="total text-center">0</span>
                                    <span class="title text-center">Fucks Given</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FOUR PANEL WRAPPER -->

                <!-- START SESSION TIMELINE ACTIVITY SECTION -->
                <!-- Half at medium - Third at large - Width at XS -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Sessions Timeline</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-expand"></i>
                                    <i class="fa fa-chevron-down"></i>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                            <div class="panel-body text-center">
                                <div>
                                    <canvas id="line" height="140"></canvas>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- END FIRST ROW -->
           <!-- LEFTOVER TEMPLATE PANEL START -->
            {{-- <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">SERVER STATS</h3>
                            <div class="reportdate actions">
                                <i class="fa fa-calendar-o"></i>
                                <span>Jan 1 - June 30</span>
                                <b class="caret"></b>
                            </div>
                        </div>
                        <div class="panel-body server-chart">
                            <div class="row">
                                <div class="col-md-12 col-lg-4">
                                    <ul>
                                        <li>
                                            <span class="text-left">Network Usage</span>
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="text-left">CPU Load</span>
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                                </div>
                                <div class="col-md-12 col-lg-8">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-6">
                                            <div class="line-chart">
                                                <canvas id="canvas1" height="100"></canvas>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-6">
                                            <div class="bar-chart">
                                                <canvas id="canvas2" height="100"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!-- LEFTOVER TEMPLATE PANEL END -->
        </section>
    </section>
    <!--main content end-->
    <script src="{{asset('js/home.js')}}"></script>

@endsection


