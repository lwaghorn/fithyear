@extends('header')

@section('content')
    <!--main content start-->
    <section class="main-content-wrapper">
        <div class="pageheader">
            <h1>Send Fake Attendance</h1>
            <p class="description">Send a sensor ID to the attendance controller to test the attendance functionality</p>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li class="active"> Attendance > Fake Submission</li>
                </ol>
            </div>
        </div>
        <section id="main-content">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 ">

                    <div class="row sensorList">


                       {{-- <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <span class="icon-check checkIcon text-center"></span>
                                        </div>
                                    </div>
                                    <div class="row check-icon-row">
                                        <div class="col-xs-12 text-center">
                                            BreakPad Heat
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body text-center">
                                    Armed and Ready
                                </div>
                            </div>
                        </div>--}}


                    </div>

                </div>
            </div>
        </section>







        <link rel="stylesheet" type="text/css" href="{{ asset('css/attendance.css') }}" >
        <script src="{{asset('js/attendance.js')}}"></script>

@endsection
